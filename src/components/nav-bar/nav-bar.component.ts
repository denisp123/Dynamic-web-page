import { Component, OnInit } from '@angular/core';

import { NavBarMenuItem } from '../../models/nav-bar-menu.model';
import { NAV_BAR_MENU_ITEMS } from '../../config/nav-bar-menu.constant';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.less']
})
export class NavBarComponent implements OnInit {
  items: NavBarMenuItem[];
  ngOnInit() {
    this.items = NAV_BAR_MENU_ITEMS;
  }
}
