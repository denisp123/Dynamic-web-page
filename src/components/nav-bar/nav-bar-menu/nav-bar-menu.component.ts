import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NavBarMenuItem } from '../../../models/nav-bar-menu.model';

@Component({
    selector: 'app-nav-bar-menu',
    styleUrls: ['./nav-bar-menu.component.less'],
    templateUrl: './nav-bar-menu.component.html'
})
export class NavBarMenuComponent {
  @Input() items: NavBarMenuItem[];

  constructor(private router: Router) {}

  goToPage(path) {
    this.router.navigate([path]);
  }
}
